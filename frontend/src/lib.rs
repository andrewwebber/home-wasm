#![recursion_limit = "2048"]
use anyhow::Error;
use log::info;
use wasm_bindgen::prelude::*;
use yew::format::{Json, Nothing};
use yew::prelude::*;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew_router::{components::RouterAnchor, prelude::*, switch::Permissive};
use yewtil::NeqAssign;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Foo {
    pub message: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Properties)]
pub struct Props {
    pub route: Option<String>,
}

pub struct PageNotFound {
    props: Props,
}

impl Component for PageNotFound {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        unimplemented!()
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        html! {
            <section class="hero is-danger is-bold is-large">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            { "Page not found" }
                        </h1>
                        <h2 class="subtitle">
                            { "This page does not seem to exist" }
                        </h2>
                    </div>
                </div>
            </section>
        }
    }
}

struct Skill {
    items: Vec<String>,
    name: String,
}

struct Skills {
    front_end: Skill,
    back_end: Skill,
    database: Skill,
    network: Skill,
    security: Skill,
    infrastructure: Skill,
    observability: Skill,
    cd: Skill,
}

impl Skills {
    pub fn new() -> Self {
        let frontEndSkills: Vec<&str> = vec![
            "Yew",
            "WebAssembly",
            "JAMStack",
            "ReactJS",
            "React Native",
            "Flux/Redux",
            "Electron",
            "TypeScript",
        ];

        let backEndSkills: Vec<&str> = vec![
            "Rust",
            "Golang",
            "Knative",
            "Cloud native microservices",
            "Event driven architecture",
            "Domain Driven Design",
            "CQRS (Command Query Responsibility Segregation)",
            "NodeJS",
            "C/C++",
        ];

        let databaseSkills: Vec<&str> = vec![
            "NoSQL",
            "Couchbase",
            "MongoDB",
            "Postgres",
            "SQL",
            "Object Storage (S3, CEPH, Google, Azure)",
        ];

        let networkSkills: Vec<&str> = vec![
            "GraphQL",
            "Kafka",
            "gRPC",
            "REST",
            "Pub/Sub",
            "Knative eventing",
            "RabbitMQ",
            "Cloudflare",
            "Layer 2/3 network segmentation",
            "Wireguard",
        ];

        let securitySkills: Vec<&str> = vec![
            "OAuth2",
            "JWT",
            "PKI",
            "SAML",
            "Hashicorp Vault",
            "ADFS",
            "DEX",
            "WS-*",
            "Letsencrypt",
        ];

        let infraSkills: Vec<&str> = vec![
            "Kubernetes Bare Metel since 2014",
            "Kubernetes API and Operators",
            "Managed EKS, GKE, AKS",
            "Packer",
            "Terraform",
            "Multi-datacenter architectures",
            "iPXE Boot",
            "CEPH",
            "Knative",
            "Google Cloud",
            "AWS",
            "Azure",
        ];

        let obSkills: Vec<&str> = vec![
            "OpenTelemetry",
            "Loki",
            "Envoy Proxy",
            "Prometheus",
            "Grafana",
            "Elastic Search",
        ];

        let cdSkills: Vec<&str> = vec![
            "Audit compliance (ISO certifications, penetration testing audits, theat-modeling)",
            "Kuberetes Operators",
            "Continous compliance",
            "Git",
            "Gitlab + Gitlab Pipelines",
            "Github",
            "Jira",
        ];

        Self {
            front_end: Skill {
                name: "Front end".to_string(),
                items: frontEndSkills.iter().map(|s| s.to_string()).collect(),
            },
            back_end: Skill {
                name: "Backend".to_string(),
                items: backEndSkills.iter().map(|s| s.to_string()).collect(),
            },
            database: Skill {
                name: "Database".to_string(),
                items: databaseSkills.iter().map(|s| s.to_string()).collect(),
            },
            network: Skill {
                name: "Networking".to_string(),
                items: networkSkills.iter().map(|s| s.to_string()).collect(),
            },
            security: Skill {
                name: "Security".to_string(),
                items: securitySkills.iter().map(|s| s.to_string()).collect(),
            },
            infrastructure: Skill {
                name: "Infrastructure".to_string(),
                items: infraSkills.iter().map(|s| s.to_string()).collect(),
            },
            observability: Skill {
                name: "Observability".to_string(),
                items: obSkills.iter().map(|s| s.to_string()).collect(),
            },
            cd: Skill {
                name: "Continuous Integration / Continuous Delivery".to_string(),
                items: cdSkills.iter().map(|s| s.to_string()).collect(),
            },
        }
    }
}

struct ValuesPage {
    values: Vec<&'static str>,
}

impl Component for ValuesPage {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let values = vec![
            "Experience leading projects from MVP to acquisition through relentless customer focus",
            "Extreme ownership and the courage to transform complex legacy organizations and systems",
            "Devops transformation, guiding organizations to dramatically reduce lead time down to days",
            "Setting up a continuous learning culture to out experiment the competition",
            "Establishing autonomous cross disciplined teams, empowered by self-service platforms",
            "Embracing calculated risk taking and failure as a critical source of learning",
            "Drive business decisions based on evidence and field telemetry",
        ];

        Self { values }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
        <section class="section">
                <h1 class="title">
                    <span class="span-black">{"Tech + Culture = "}</span>
                    <span class="span-pink">{"High Performing Organization"}</span>
                </h1>
            {
                self.values.iter().map(| value| html!{
            <article class="message is-info">
                <div class="message-body">
                {value}
                </div>
            </article>
                }).collect::<Html>()
            }
           <footer class="footer">
             <p> {"Written in 🦀 + 🕸️ Assembly"} </p>
           </footer>

        </section>
        }
    }
}

struct TechSkillsPage {
    skills: Skills,
}

impl Component for TechSkillsPage {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            skills: Skills::new(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
        <section class="section">
            <h1 class="title">
                <span class="span-black">{"Tech + Culture = "}</span>
                <span class="span-pink">{"High Performing Organization"}</span>
            </h1>
            <section class="section">
                <h1 class="title">{"Full Stack"}</h1>
                <div class="columns">
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.front_end.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.front_end.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.back_end.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.back_end.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.database.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.database.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section">
                <h1 class="title">{"Application Lifecycle Management"}</h1>
                <div class="columns">
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.observability.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.observability.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.cd.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.cd.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section">
                <h1 class="title">{"Public / Private Cloud Experience"}</h1>
                <div class="columns">
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.infrastructure.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.infrastructure.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.security.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.security.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <div class="card-content">
                                <div class="content">
                                    <article class="message is-dark">
                                      <div class="message-header">
                                        <p>{&self.skills.network.name}</p>
                                      </div>
                                      <div class="message-body">
                                        <ul>
                                        {
                                            self.skills.network.items.iter().map(|skill | html!{
                                                <li>{skill}</li>
                                            }).collect::<Html>()
                                        }
                                        </ul>
                                      </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer">
              <p> {"Written in 🦀 + 🕸️ Assembly"} </p>
            </footer>
        </section>
        }
    }
}

struct ContactPage {}

impl Component for ContactPage {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <section class="section">
                <h1 class="title">
                    <span class="span-black">{"Tech + Culture = "}</span>
                    <span class="span-pink">{"High Performing Organization"}</span>
                </h1>
                <div class="column contact is-flex is-justify-content-center">
                    <img class="contact-picture" src="images/awebber-grey.jpg" alt="andrew webber"/>
                </div>
                <div class="columns">
                    <div class="column contact is-flex is-justify-content-center">
                        <a href="mailto:andrewvwebber@googlemail.com">
                            <img src="images/icon-mail.png" alt="email"/>
                        </a>
                        <a href="https://www.linkedin.com/in/webberandrew/">
                            <img src="images/icon-linkedin.png" alt="linkedin"/>
                        </a>
                        <a href="https://twitter.com/andrewvwebber">
                            <img src="images/icon-twitter.png" alt="twitter"/>
                        </a>
                        <a href="https://gitlab.com/andrewwebber">
                            <img src="images/icon-gitlab.png" alt="gitlab"/>
                        </a>
                        <a href="https://github.com/andrewwebber">
                            <img src="images/icon-github.png" alt="github"/>
                        </a>
                        <a href="https://www.instagram.com/andrewvwebber/">
                            <img src="images/icon-instagram.png" alt="instagram"/>
                        </a>
                    </div>
                </div>
                <footer class="footer">
                  <p> {"Written in 🦀 + 🕸️ Assembly"} </p>
                </footer>
            </section>
        }
    }
}

struct Book {
    title: &'static str,
    image: &'static str,
    url: &'static str,
}

struct Books {
    books: Vec<Book>,
}

impl Books {
    pub fn new() -> Self {
        let books = vec![
           Book{ title: "Radial Focus", image: "images/book-radical.jpg", url: "https://www.amazon.com/Radical-Focus-Achieving-Important-Objectives-ebook/dp/B01BFKJA0Y"},
           Book{ title: "Measure What Matters", image: "images/book-measure.jpeg", url: "https://www.amazon.de/John-Doerr/dp/024134848X/"},
           Book{ title: "The Lean Startup", image: "images/book-lean.jpg", url: "https://www.amazon.de/Lean-Startup-Entrepreneurs-Continuous-Innovation/dp/1524762407"},
           Book{ title: "The Culture Code", image: "images/book-culture-code.jpg", url: "https://www.amazon.de/Culture-Code-Secrets-Highly-Successful/dp/152479709X"},
           Book{ title: "Accelerate", image: "images/book-accelerate.jpg", url: "https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM"},
           Book{ title: "Team Topologies", image: "images/book-teamtopologies.jpg", url: "https://www.amazon.com/Team-Topologies-Organizing-Business-Technology-ebook/dp/B07NSF94PC/"},
           Book{ title: "The DevOps Handbook", image: "images/book-devopshandbook.jpg", url: "https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations-ebook/dp/B01M9ASFQ3"},
           Book{ title: "The Unicorn Project", image: "images/book-unicornproject.jpg", url: "https://www.amazon.com/Unicorn-Project-Developers-Disruption-Thriving-ebook/dp/B07QT9QR41/"},
           Book{ title: "The Phoenix Project", image: "images/book-phoenixproject.jpg", url: "https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B078Y98RG8"},
           Book{ title: "Microservices Patterns", image: "images/book-microservices.jpg", url: "https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B078Y98RG8"},
           Book{ title: "Designing Data-intensive Applications", image: "images/book-data.jpg", url: "https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable/dp/1449373321"},
           Book{ title: "The art of scalability", image: "images/book-scalability.jpg", url: "https://www.amazon.com/Art-Scalability-Architecture-Organizations-Enterprise/dp/0134032802/"},
           Book{ title: "Site Reliability Engineering", image: "images/book-sre.jpg", url: "https://www.amazon.com/Site-Reliability-Engineering-Production-Systems/dp/149192912X"},
           Book{ title: "The Culture Game", image: "images/book-culture.jpg", url: "https://www.amazon.com/Culture-Game-Tools-Agile-Manager/dp/0984875301"},
           Book{ title: "Testing Business Ideas", image: "images/book-testbusiness.jpg", url: "https://www.amazon.com/Rapid-Testing-Business-Ideas-Customer/dp/1119551447"},
           Book{ title: "Project to Product", image: "images/book-project.jpg", url: "https://www.amazon.com/Project-Product-Survive-Disruption-Framework/dp/1942788398"},
           Book{ title: "Extreme Ownership", image: "images/book-extreme.jpg", url: "https://www.amazon.com/Extreme-Ownership-U-S-Navy-SEALs-ebook/dp/B00VE4Y0Z2"},
        ];
        Self { books }
    }
}

struct SciencePage {
    books: Books,
}

impl Component for SciencePage {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            books: Books::new(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
        <section class="section">
            <h1 class="title">
                <span class="span-black">{"Tech + Culture = "}</span>
                <span class="span-pink">{"High Performing Organization"}</span>
            </h1>
            <div class="book-shelf">
            {
                self.books.books.iter().map(|book| html!{
                <div class="book">
                    <a href={book.url}>
                        <img src={book.image} alt={book.title} />
                    </a>
                </div>
                }).collect::<Html>()
            }
            </div>
            <footer class="footer">
              <p> {"Written in 🦀 + 🕸️ Assembly"} </p>
            </footer>
        </section>
        }
    }
}

enum HomeMsg {
    Ignore,
    DataFetched(Result<Foo, Error>),
}

struct HomePage {
    link: ComponentLink<Self>,
    ft: Option<FetchTask>,
    fetching: bool,
    data: Option<Foo>,
}

impl Component for HomePage {
    type Message = HomeMsg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        // let callback = link.callback(move |response: Response<Json<Result<Foo, Error>>>| {
        //     let (meta, Json(data)) = response.into_parts();
        //     info!("META: {:?}, {:?}", meta, data);
        //     if meta.status.is_success() {
        //         HomeMsg::DataFetched(data)
        //     } else {
        //         HomeMsg::Ignore // FIXME: Handle this error accordingly.
        //     }
        // });
        // let request = Request::get("/api/hello/frontend").body(Nothing).unwrap();

        // let ft = Some(FetchService::fetch(request, callback).unwrap());
        Self {
            link,
            ft: None,
            fetching: false,
            data: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            HomeMsg::DataFetched(response) => {
                self.fetching = false;
                self.data = response.ok();
                true
            }
            _ => false,
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
        <section class="section">
            <h1 class="title">
                <span class="span-black">{"Tech + Culture = "}</span>
                <span class="span-pink">{"High Performing Organization"}</span>
            </h1>
            <div class="columns is-vcentered">
                <div class="column is-6">
                    <p class="is-size-1">{"Andrew Webber"}</p>
                    <p class="is-size-1">{"Full Stack Cloud Engineer"}</p>
                </div>
                <div class="column is-6">
                    <img src="images/awebber-grey.jpg" />
                </div>
            </div>
            <h2 class="title">
                <span class="span-black">{"Accelerate from "}</span>
                <span class="span-pink">{"MVP"}</span>
                <span class="span-black">{" to acquisition 🚀"}</span>
            </h2>
            <h3 class="subtitle">
            {"In today's markets, it's no longer big vs. small. It's slow vs
                fast."}
            </h3>
            <div class="columns has-text-centered">
                <div class="column is-2 title-link">
                        <RouterAnchor<AppRoute>  route=AppRoute::Values>
                            { "Strategy" }
                        </RouterAnchor<AppRoute>>
                </div>
                <div class="column is-2 title-link">
                        <RouterAnchor<AppRoute> route=AppRoute::Science>
                            { "Science" }
                        </RouterAnchor<AppRoute>>
                </div>
                // <div class="column is-2 title-link"><a href="">{"Tactics"}</a></div>
                <div class="column is-2 title-link">
                        <RouterAnchor<AppRoute> route=AppRoute::TechSkills>
                            { "Tech Skills" }
                        </RouterAnchor<AppRoute>>
                </div>
                <div class="column is-2 title-link">
                        <RouterAnchor<AppRoute> route=AppRoute::Contact>
                            { "Contact" }
                        </RouterAnchor<AppRoute>>
                </div>
            </div>
            <footer class="footer">
              <p> {"Written in 🦀 + 🕸️ Assembly"} </p>
            </footer>
        </section>
        }
    }
}

#[derive(Switch, Clone, Debug)]
pub enum AppRoute {
    #[to = "/values"]
    Values,
    #[to = "/tech-skills"]
    TechSkills,
    #[to = "/contact"]
    Contact,
    #[to = "/science"]
    Science,
    #[to = "/page-not-found"]
    PageNotFound(Permissive<String>),
    #[to = "/"]
    Index,
}

struct Model {}

impl Component for Model {
    type Message = ();
    type Properties = ();
    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        type AppRouter = Router<AppRoute>;
        html! {
            <AppRouter
                render=AppRouter::render(Self::switch),
                redirect=Router::redirect(|route: Route| {
                    AppRoute::PageNotFound(Permissive(Some(route.route)))
                })
            />
        }
    }
}

impl Model {
    fn switch(switch: AppRoute) -> Html {
        log::info!("Switch: {:?}", switch);
        match switch {
            AppRoute::Index => {
                html! { <HomePage /> }
            }
            AppRoute::Values => {
                html! { <ValuesPage /> }
            }
            AppRoute::TechSkills => {
                html! { <TechSkillsPage /> }
            }
            AppRoute::Contact => {
                html! { <ContactPage /> }
            }
            AppRoute::Science => {
                html! { <SciencePage /> }
            }

            AppRoute::PageNotFound(Permissive(route)) => {
                html! { <PageNotFound route=route /> }
            }
        }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    wasm_logger::init(wasm_logger::Config::default());
    App::<Model>::new().mount_to_body();
}
