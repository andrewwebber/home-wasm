IMAGE=eu.gcr.io/origins-containers/andrewwebber:v2.0.11

all: build

build:
	cargo build
	(cd ./frontend; wasm-pack build --target web --out-name wasm --out-dir ./static)

release:
	cargo audit
	cargo build --release --target x86_64-unknown-linux-musl

run:
	cargo run

docker-image: release
	docker build --network=host --no-cache -t $(IMAGE) .

docker-push: docker-image
	gcloud docker -- push $(IMAGE)

deploy: docker-push
	gcloud run deploy andrewwebber --image $(IMAGE) --platform managed --project origins-containers --region europe-west1
