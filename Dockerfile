FROM scratch
COPY ./target/x86_64-unknown-linux-musl/release/backend /usr/local/bin/
COPY ./frontend ./frontend
USER 1001
EXPOSE 8080
ENTRYPOINT ["backend"]
